import java.util.Scanner;

public class ex2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a number: ");
        int number = scanner.nextInt();

        if (number < 0) {
            System.out.println("You have a negative number");
        } else System.out.println("You have a positive number");
    }

}
