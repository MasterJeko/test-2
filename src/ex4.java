import java.util.Scanner;

public class ex4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a number: ");
        int number1 = scanner.nextInt();
        System.out.println("Input another number: ");
        int number2 = scanner.nextInt();
        System.out.println("Input the last number: ");
        int number3 = scanner.nextInt();

        if (number2 == number3 && number1 == number3) {
            System.out.println("Toate numerele sunt egale");
        } else if (number1 != number2 && number2 != number3 && number1 != number3) {
            System.out.println("Toate numerele sunt diferite");
        } else System.out.println("Nici nu sunt egale, nici diferite");
    }
}
