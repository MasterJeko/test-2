package ex13;

public class Clasa1part2 {

    private int nr1 = 10;
    private int nr2 = 20;
    private int sum = nr1 + nr2;

    public int getSum() {
        return sum;
    }

    public void setNr1(int nr1) {
        this.nr1 = nr1;
    }

    public void setNr2(int nr2) {
        this.nr2 = nr2;
    }
}
