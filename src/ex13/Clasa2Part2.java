package ex13;

public class Clasa2Part2 {

    static Clasa1part2 clasa1part2 = new Clasa1part2();

    public static void calculate() {
        System.out.println(clasa1part2.getSum());
    }

    public static void calculate(int number1 , int number2) {
        clasa1part2.setNr1(number1);
        clasa1part2.setNr2(number2);
        int totalSum = number1 + number2;
        System.out.println(totalSum);
    }

}
